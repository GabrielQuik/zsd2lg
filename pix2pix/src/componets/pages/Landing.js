import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import Select from 'react-select'
import axios from "axios";
import FormData from 'form-data'
import Navbar from '../Navbar'
import Footer from '../Footer'

export default function Landing() {
    return (
        <div>
        <Navbar/>
        <center id='title'> Założenia Projektowe </center>
            <div class="flex-zalozenia">
            <ul>
                <li>dla konturów interfejs rysowania czarno-białych szkiców (gumka, ołówek, grubości rysowania, kształt: kółko, kwadrat)</li>
                <li>dla segmentów interfejs z doborem kolorów jako części twarzy (nos, usta, oczy...)</li>
                <li>operowanie na ludzkiej twarzy</li>
                <li>funkcjonalność kontur i segmentów działająca w obie strony (tutaj działanie w obie strony jest trzecią funkcjonalnością)</li>
                <li>implementacja algorytmu segmentacji twarzy</li>
            </ul>
            <ul>
                <li> próba rozbudowania Pix2Pix o kolejne warstwy</li>
                <li>aplikacja webowa obsługująca wybieranie trybów pracy</li>
                <li>aplikacja nie zakłada wytrzymania obciążenia powyżej 100 clientów</li>
                <li>brak certyfikatu HTTPS</li>
                <li>brak możliwości zapisu obrazów do bazy danych</li>
            </ul>
            </div>
            <center id='title'> Stack technologiczny </center>
            <div class="flex-zalozenia">
            <ul class='three'>
                <li id='python_logo'>Python</li>
                <li id='go_logo'>Go</li>
                <li id='react_logo'>React.js</li>
            </ul>
            </div>
            <center id='title'> Zespół </center>
            <center><img src="team2.png" class="centerImage"/> </center>
            <h1 id='leader'>Gabriel Kuik</h1>
            <div class="flex-zespol">
            <ul>
                <li id='python_logo'>Mateusz Kolimaga</li>
                <li id='python_logo'>Michał Ślizewski</li>
                <li id='python_logo'>Krystian Tatara</li>
                <li id='python_logo'>Krzysztof Walentukiewicz</li>
            </ul>
            <ul>
                <li id='go_logo'>Maksymilian Lis</li>
                <li id='go_logo'>Albert Masiak</li>
            </ul>
            <ul>
                <li id='react_logo'>Kamil Duszyński</li>
                <li id='react_logo'>Mateusz Gajos</li>
            </ul>
            </div>
         <Footer/>

        </div>
    )
}