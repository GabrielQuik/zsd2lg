Przed uruchomieniem należy pobrać model z dysku google:

Pix2Pix/f2c.zip

rozpakować model i ścieżkę rozpakowanego modelu podmienić w f2c.py w 6 linijce:

GENERATOR_PATH = 'path/to/file/generator_f2c.h5'

Uruchamianie skryptu f2c.py:

python f2c.py *path_to_source_file  path_to_dest_file*

gdzie: *path_to_source_file* - to ścieżka do pliku który ma być przetwarzany przez pix2pixa *path_to_dest_file* - to ścieżka do pliku przetworzonego

przykład: python f2c.py in.jpg out.jpg
