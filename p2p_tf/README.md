Przed uruchomieniem należy zainstalować checkpointy z dysku google:

training_checkpoints_BA oraz training_checkpoints_AB dla segmentów (_segments_new) oraz landmarków (_landmarks)

i umieścić je w tym samym folderze:

Uruchamianie skryptu test.py:

python test.py <em>path_to_source_file  path_to_dest_file  mode  direction</em>

gdzie:
<em>path_to_source_file</em> - to ścieżka do pliku który ma być przetwarzany przez pix2pixa 
<em>path_to_dest_file</em> - to ścieżka do pliku przetworzonego
<em>mode</em> - wybór przetwarzania segmentowego lub konturowego ('seg' albo 'con')
<em>path_to_dest_file</em> - segmenty/kontury na twarze ('AB') lub odwrotnie ('BA')

przykład:
python test.py in.jpg out.jpg seg AB
