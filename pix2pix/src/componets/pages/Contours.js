import React, { useEffect, useLayoutEffect, useRef, useState } from "react";

import axios from "axios";
import Select from 'react-select'
import Navbar from "../Navbar"
import Footer from "../Footer"

var brushType = 'pen';

export default function Contours() {
    const canvasRef = useRef(null)
    const contextRef = useRef(null)
    const [isDrawing, setIsDrawing] = useState(false)
    const options = [
        { value: 'square', label: 'Square' },
        { value: 'pen', label: 'Pen' },
        { value: 'circle', label: 'Circle' }]

    useEffect(() => {
        const canvas = canvasRef.current;
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        let x = canvas.width/512;
        let y = canvas.height/512;
        canvas.style.width = `${window.innerWidth/x}px`;
        canvas.style.height = `${window.innerHeight/y}px`;
        console.log(canvas.width, canvas.height)
        const context = canvas.getContext("2d")
        context.scale(x, y)
        context.lineCap = "round"
        context.strokeStyle = "black"
        context.lineWidth = 5
        contextRef.current = context;
        }, [])
    

    const startDrawing = ({ nativeEvent }) => {
        const { offsetX, offsetY } = nativeEvent;
        contextRef.current.beginPath()
        contextRef.current.moveTo(offsetX, offsetY)
        setIsDrawing(true)
    }
    const endDrawing = () => {
        contextRef.current.closePath()
        setIsDrawing(false)
    }

    const draw = ({ nativeEvent }) => {
        if (!isDrawing) {
            return
        }
        const { offsetX, offsetY } = nativeEvent;
        if (brushType === 'pen') {
            contextRef.current.lineTo(offsetX, offsetY)
            contextRef.current.stroke()
        }
        else if (brushType === 'square') {
            contextRef.current.beginPath()
            contextRef.current.fillRect(offsetX - contextRef.current.lineWidth, offsetY
                - contextRef.current.lineWidth, contextRef.current.lineWidth, contextRef.current.lineWidth);
            contextRef.current.stroke();
        }
        else if (brushType === 'circle') {
            contextRef.current.beginPath()
            contextRef.current.arc(offsetX, offsetY, contextRef.current.lineWidth / 4, 0, Math.PI * 2, false);
            contextRef.current.stroke();
        }
    }
    const incrementWidth = () => {
        contextRef.current.lineWidth = contextRef.current.lineWidth + 1
    }
    const decrementWidth = () => {
        contextRef.current.lineWidth = contextRef.current.lineWidth - 1
    }
    const changeBrushType = e => {
        brushType = e.value
    }
    const clear = () => {
        contextRef.current.fillStyle = "white"
        contextRef.current.fillRect(0, 0, contextRef.current.canvas.width, contextRef.current.canvas.height);
        document.getElementById("out").classList.remove("result_image");
    }
    const dowlnoad = async () => {
        var canvas = canvasRef.current
        canvas.toBlob(function(blob) {
            const formData = new FormData();
            formData.append('image', blob, 'filename.png');
            formData.append('mode', 4);
            axios.post('http://localhost:4000/', formData).then(() => {
                document.getElementById("out").classList.add("result_image");
            });
        });
    }

    const hiddenFileInput = React.useRef(null);

    const transform = event => {
        hiddenFileInput.current.click();  
    }

    const handleChange = event => {
        const fileUploaded = event.target.files[0];
        const formData = new FormData();
        formData.append('image', fileUploaded);
        formData.append('mode', 3)
        axios.post('http://localhost:4000/', formData).then(() => {document.getElementById("out").classList.add("result_image")})
    };

    const canvas_style = {
            border: "1px solid #000000",
            backgroundColor: "white",
            padding: "10px",
            fontFamily: "Arial",
          };

    return (
        <div>
        <body>
            <Navbar/>
            <div id="tools">
            <table>
                <tr>
                    <td></td>
                    Choose line style:
                    <div style={{ width: '300px' }}>
                        <Select options={options}
                            onChange={changeBrushType}
                        />
                    </div>
                    <td>
                        Line Width
                        <button onClick={incrementWidth}>+</button>
                        <button onClick={decrementWidth}>-</button></td>
                    <td><button onClick={clear}>Clear</button></td>
                    <td><button onClick={transform}>Transfrom from upload</button></td>
                    <input
                        type="file"
                        ref={hiddenFileInput}
                        onChange={handleChange}
                        style={{display: 'none'}}
                    />
                    <td> <button onClick={dowlnoad}>Transform from canvas</button> </td>
                </tr>
            </table>
            </div>

            <div style = {{display: 'flex', justifyContent: 'center', margin: '30px 20px 0 0'}}>
            <canvas style={canvas_style}
                onMouseDown={startDrawing}
                onMouseMove={draw}
                onMouseUp={endDrawing}
                ref={canvasRef}>
            </canvas>
            <div id ="out" className="result_frame"/>
            </div>

        </body>
        <Footer/>
        </div>
    )
}
