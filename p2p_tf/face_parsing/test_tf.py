#!/usr/bin/python
# -*- encoding: utf-8 -*-

from model import BiSeNet

import torch

import os
import os.path as osp
import numpy as np
from PIL import Image
import torchvision.transforms as transforms
import cv2


def vis_parsing_maps(parsing_anno, stride, save_im=False, save_path='vis_results/parsing_map_on_im.jpg'):
    """function get parsed image and save it or not
    :param parsing_anno: image that will be parsed
    :type  parsing_anno: PIL RGB image
    :param stride: stride of the convolving kernel
    :type stride: int
    :param save_im: parameter define if function will save proceeded image or not
    :type save_im: bool
    :param save_path: path for saving images
    :type save_im: str
    """
    part_colors = [[255, 0, 0], [255, 85, 0], [255, 170, 0],
                   [255, 0, 85], [255, 0, 170],
                   [0, 255, 0], [85, 255, 0], [170, 255, 0],
                   [0, 255, 85], [0, 255, 170],
                   [123, 0, 0], [85, 0, 255], [170, 0, 255],
                   [0, 85, 255], [0, 170, 255],
                   [255, 255, 0], [255, 255, 85], [255, 255, 170],
                   [255, 0, 255], [255, 85, 255], [255, 170, 255],
                   [0, 255, 255], [85, 255, 255], [170, 255, 255]]

    vis_parsing_anno = parsing_anno.copy().astype(np.uint8)
    vis_parsing_anno = cv2.resize(vis_parsing_anno, None, fx=stride, fy=stride, interpolation=cv2.INTER_NEAREST)
    vis_parsing_anno_color = np.zeros((vis_parsing_anno.shape[0], vis_parsing_anno.shape[1], 3)) + 255

    num_of_class = np.max(vis_parsing_anno)

    for pi in range(1, num_of_class + 1):
        index = np.where(vis_parsing_anno == pi)
        vis_parsing_anno_color[index[0], index[1], :] = part_colors[pi]

    vis_parsing_anno_color = vis_parsing_anno_color.astype(np.uint8)

    # Save result or not
    if save_im:
        cv2.imwrite(save_path[:-4] + '.png', vis_parsing_anno_color)


def evaluate(respth='./data_out', dspth='./data', cp='79999_iter.pth'):
    """function call vis_parsing_map function for every image in respath
    :param respath: path to directory with images to proceed
    :type  respath: str
    :param dspath: path to directory with saved proceeded images
    :type dspath: str
    :param cp: path to model
    :type cp: str
    """
    if not os.path.exists(respth):
        os.makedirs(respth)

    n_classes = 19
    net = BiSeNet(n_classes=n_classes)
    save_pth = cp
    net.load_state_dict(torch.load(save_pth, map_location=torch.device('cpu')))
    net.eval()

    to_tensor = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    done = len(os.listdir(respth))
    with torch.no_grad():
        for image_path in sorted(os.listdir(dspth))[0:]:
            img = Image.open(osp.join(dspth, image_path))
            image = img.resize((512, 512), Image.BILINEAR)
            img = to_tensor(image)
            img = torch.unsqueeze(img, 0)
            out = net(img)[0]
            parsing = out.squeeze(0).cpu().numpy().argmax(0)
            print(image_path)
            vis_parsing_maps(parsing, stride=1, save_im=True, save_path=osp.join(respth, image_path))


if __name__ == "__main__":
    evaluate(dspth='data_in', cp='79999_iter.pth')
