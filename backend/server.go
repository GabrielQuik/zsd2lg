package main

import (
	"backend/controller"
	"backend/service"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	log.Println("Starting server")
	app := fiber.New()
	app.Use(cors.New())
	serverService := service.NewServerService()
	serverController := controller.NewServerController(serverService)
	serverController.RegisterPath(app)
	log.Fatal(app.Listen(":4000"))
}
