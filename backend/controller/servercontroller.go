package controller

import (
	"backend/service"

	"github.com/gofiber/fiber/v2"
)

type ServerController struct {
	service *service.ServerService
}

func NewServerController(serv *service.ServerService) *ServerController {
	serverController := new(ServerController)
	serverController.service = serv
	return serverController
}

func (controller ServerController) RegisterPath(app *fiber.App) {
	app.Post("/", controller.Response)
}

func (controller ServerController) Response(c *fiber.Ctx) error {
	err := controller.service.Write(c)
	return err
}
