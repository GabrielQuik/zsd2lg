# Pix2Pix

### Założenia:
 - [ ] dla konturów interfejs rysowania czarno-białych szkiców (gumka, ołówek, grubości rysowania, kształt[kółko, kwadrat])
 - [ ] dla segmentów interfejs z doborem kolorów jako części twarzy (nos, usta, oczy...)
 - [ ] operowanie na ludzkiej twarzy
 - [ ] funkcjonalność kontur i segmentów działająca w obie strony (tutaj działanie w obie strony jest trzecią funkcjonalnością)
 - [ ] implementacja algorytmu segmentacji twarzy
 - [ ] brak możliwości nakładania dodatków/akcesoriów (biżuteria itp.)
 - [ ] próba rozbudowania Pix2Pix o kolejne warstwy
 - [ ] aplikacja webowa obsługująca wybieranie trybów pracy
 - [ ] aplikacja nie zakłada wytrzymania obciążenia powyżej 100 clientów
 - [ ] brak certyfikatu HTTPS
 - [ ] brak możliwości zapisu obrazów do bazy danych

### Technology Stack:
- ReactJs
- Go
- Python3

### Frontend
#### Instrukcja uruchamiania aplikacji React:
- Zainstalować nodejs
  - curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
  - sudo bash nodesource_setup.sh
  - sudo apt-get install nodejs -y 
- Przejść do folderu pix2pix i uruchomić aplikację
  - npm install
  - npm start

### Backend
#### Instrukcja uruchamiania serwera:
- Instalacja GO
  - pobrać go ze strony https://golang.org/doc/install
  - sudo rm -rf /usr/local/go && tar -C /usr/local -xzf go1."golang version".linux-amd64.tar.gz
  - echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.profile
  - source ~/.profile
- Uruchomianie serwera
  - przejść do folderu backend
  - go mod download
  - go run server.go
  - http://localhost:3000/
