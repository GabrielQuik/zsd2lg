export default function Footer() {
    return (
        <footer>
          <div class="flex-footer">
            <ul class='three'>
                <a href="https://gitlab.com/GabrielQuik/zsd2lg"><li id='gitlab_logo'>Gitlab</li> </a>
                <a href="/"><li id='home_logo'>Strona Główna</li> </a>
                <a href="mailto:laughgrade69@gmail.com"> <li id='gmail_logo'>Email</li> </a>
            </ul>
          </div>
        </footer>
        )
}