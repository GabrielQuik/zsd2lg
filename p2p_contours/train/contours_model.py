from keras import Input, Model
from keras.layers import Conv2D
from keras.layers import BatchNormalization
from keras.layers import Dropout
from keras.layers import concatenate
from keras.layers import Activation
from keras.layers import Conv2DTranspose
from keras.layers.advanced_activations import LeakyReLU
from tensorflow.python.ops.init_ops_v2 import RandomNormal
from keras.optimizer_v2.adam import Adam


def generator(input_shape, output_channels):
    # U-Net
    # Hyperparameters
    kernel_size = 4
    strides = 2
    leakyrelu_alpha = 0.2
    dropout = 0.5
    init = RandomNormal(0, 0.02)

    # Architecture
    # Encoder
    input = Input(shape=input_shape)
    encoder1 = Conv2D(
        filters=64,
        kernel_size=kernel_size, 
        strides=strides,
        padding='same',
        kernel_initializer=init)(input)
    encoder1 = LeakyReLU(alpha=leakyrelu_alpha)(encoder1)
    
    encoder2 = Conv2D(
        filters=128, 
        kernel_size=kernel_size,
        strides=strides,
        padding='same',
        kernel_initializer=init)(encoder1)
    encoder2 = BatchNormalization()(encoder2, training=True)
    encoder2 = LeakyReLU(alpha=leakyrelu_alpha)(encoder2)
    
    encoder3 = Conv2D(
        filters=256, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder2)
    encoder3 = BatchNormalization()(encoder3, training=True)
    encoder3 = LeakyReLU(alpha=leakyrelu_alpha)(encoder3)
    
    encoder4 = Conv2D(
        filters=512, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder3)
    encoder4 = BatchNormalization()(encoder4, training=True)
    encoder4 = LeakyReLU(alpha=leakyrelu_alpha)(encoder4)

    encoder5 = Conv2D(
        filters=512, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder4)
    encoder5 = BatchNormalization()(encoder5, training=True)
    encoder5 = LeakyReLU(alpha=leakyrelu_alpha)(encoder5)

    encoder6 = Conv2D(
        filters=512, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder5)
    encoder6 = BatchNormalization()(encoder6, training=True)
    encoder6 = LeakyReLU(alpha=leakyrelu_alpha)(encoder6)

    encoder7 = Conv2D(
        filters=512, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder6)
    encoder7 = BatchNormalization()(encoder7, training=True)
    encoder7 = LeakyReLU(alpha=leakyrelu_alpha)(encoder7)

    encoder8 = Conv2D(
        filters=512, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder7)
    encoder8 = BatchNormalization()(encoder8, training=True)
    encoder8 = LeakyReLU(alpha=leakyrelu_alpha)(encoder8)

    # Decoder
    decoder1 = Conv2DTranspose(
        filters=512, 
        kernel_size=kernel_size,
        strides=strides, 
        padding='same',
        kernel_initializer=init)(encoder8)
    decoder1 = BatchNormalization()(decoder1, training=True)
    decoder1 = Dropout(dropout)(decoder1, training=True)
    decoder1 = concatenate([decoder1, encoder7], axis=3)
    decoder1 = Activation('relu')(decoder1)

    decoder2 = Conv2DTranspose(
        filters=512, 
        kernel_size=kernel_size,
        strides=strides,
        padding='same',
        kernel_initializer=init)(decoder1)
    decoder2 = BatchNormalization()(decoder2, training=True)
    decoder2 = Dropout(dropout)(decoder2, training=True)
    decoder2 = concatenate([decoder2, encoder6])
    decoder2 = Activation('relu')(decoder2)

    decoder3 = Conv2DTranspose(
        filters=512, 
        kernel_size=kernel_size,
        strides=strides,  
        padding='same',
        kernel_initializer=init)(decoder2)
    decoder3 = BatchNormalization()(decoder3, training=True)
    decoder3 = Dropout(dropout)(decoder3, training=True)
    decoder3 = concatenate([decoder3, encoder5])
    decoder3 = Activation('relu')(decoder3)

    decoder4 = Conv2DTranspose(
        filters=512, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(decoder3)
    decoder4 = BatchNormalization()(decoder4, training=True)
    decoder4 = concatenate([decoder4, encoder4])
    decoder4 = Activation('relu')(decoder4)


    decoder5 = Conv2DTranspose(
        filters=256, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(decoder4)
    decoder5 = BatchNormalization()(decoder5, training=True)
    decoder5 = concatenate([decoder5, encoder3])
    decoder5 = Activation('relu')(decoder5)

    decoder6 = Conv2DTranspose(
        filters=128, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(decoder5)
    decoder6 = BatchNormalization()(decoder6, training=True)
    decoder6 = concatenate([decoder6, encoder2])
    decoder6 = Activation('relu')(decoder6)

    decoder7 = Conv2DTranspose(
        filters=64, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(decoder6)
    decoder7 = BatchNormalization()(decoder7, training=True)
    decoder7 = concatenate([decoder7, encoder1])
    decoder7 = Activation('relu')(decoder7)

    decoder8 = Conv2DTranspose(
        filters=output_channels, 
        kernel_size=kernel_size, 
        strides=strides, 
        padding='same',
        kernel_initializer=init)(decoder7)
    decoder8 = Activation('tanh')(decoder8)

    model = Model(
        inputs=[input], 
        outputs=[decoder8])

    return model


def discriminator(image_shape_source, imgage_shape_target):
    # PatchGAN
	init_weights = RandomNormal(0, 0.02)

    # Architecture
	in_source_image = Input(shape=image_shape_source)
	in_target_image = Input(shape=imgage_shape_target)
	merged = concatenate([in_source_image, in_target_image])

	d = Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init_weights)(merged)
	d = LeakyReLU(alpha=0.2)(d)

	d = Conv2D(128, (4,4), strides=(2,2), padding='same', kernel_initializer=init_weights)(d)
	d = BatchNormalization()(d)
	d = LeakyReLU(alpha=0.2)(d)

	d = Conv2D(256, (4,4), strides=(2,2), padding='same', kernel_initializer=init_weights)(d)
	d = BatchNormalization()(d)
	d = LeakyReLU(alpha=0.2)(d)

	d = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init_weights)(d)
	d = BatchNormalization()(d)
	d = LeakyReLU(alpha=0.2)(d)

	d = Conv2D(512, (4,4), padding='same', kernel_initializer=init_weights)(d)
	d = BatchNormalization()(d)
	d = LeakyReLU(alpha=0.2)(d)

	d = Conv2D(1, (4,4), padding='same', kernel_initializer=init_weights)(d)
	patch_out = Activation('sigmoid')(d)

	model = Model(
        inputs=[in_source_image, in_target_image], 
        outputs=[patch_out])

	opt = Adam(lr=0.0002, beta_1=0.5)
	model.compile(loss='binary_crossentropy', optimizer=opt, loss_weights=[0.5])

	return model


def GAN(generator_model, discriminator_model, image_shape_source):
    # cGAN
    for layer in discriminator_model.layers:
    	if not isinstance(layer, BatchNormalization):
    		layer.trainable = False
    
    # Architecture
    in_source = Input(shape=image_shape_source)
    generator_out = generator_model(in_source)
    discriminator_out = discriminator_model([in_source, generator_out])

    model = Model(
        inputs=[in_source], 
        outputs=[discriminator_out, generator_out])

    opt = Adam(lr=0.0002, beta_1=0.5)
    model.compile(loss=['binary_crossentropy', 'mae'], optimizer=opt, loss_weights=[1,100])

    return model

