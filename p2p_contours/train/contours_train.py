from keras.models import load_model
import numpy as np
import cv2

import contours_model
import prepare_dataset
import f2c

# If True then train contours to faces, if False then train faces to contours
C2F = False

# If True then create new models, otherwise load existing models
FIRST_TRAINING = False

# Paths
CONTOURS_PATH = 'landmarks'
FACES_PATH = 'data1024x1024'

GENERATOR_LOAD_PATH = 'results/generator.h5'
DISCRIMINATOR__LOAD_PATH = 'results/discriminator.h5'

GENERATOR_SAVE_PATH = 'results/generator.h5'
DISCRIMINATOR__SAVE_PATH = 'results/discriminator.h5'

TEST_IMG_LOAD_PATH = 'landmarks/hed_29999.jpg'
TEST_IMG_SAVE_PATH = '/content/drive/MyDrive/GAN/results'

# Hyperparameters
TRAIN_SAMPLES = 200
BATCH_SIZE = 1
EPOCHS = 100


def train(g_model, d_model, gan_model, dataset, epochs=1, batch_size=1, patch_N=16):
	steps = int(dataset[0].shape[0] / batch_size)

	labels_real = np.ones((batch_size, patch_N, patch_N, 1))
	labels_fake = np.zeros((batch_size, patch_N, patch_N, 1))

	for epoch in range(epochs):
		randomize = np.arange(dataset[0].shape[0])
		np.random.shuffle(randomize)
		dataset[0] = dataset[0][randomize]
		dataset[1] = dataset[1][randomize]

		train_contours_batches = np.array(np.split(dataset[0], steps))
		train_faces_batches = np.array(np.split(dataset[1], steps))
		for step in range(steps):
			print(f'Epoch: {epoch + 1}/{epochs}   Step: {step + 1}/{steps}.')

			if C2F == True:
				generator_out = g_model.predict(train_contours_batches[step])

				d_loss1 = d_model.train_on_batch(
					[train_contours_batches[step], train_faces_batches[step]], 
					labels_real)

				d_loss2 = d_model.train_on_batch(
					[train_contours_batches[step], generator_out], 
					labels_fake)

				g_loss, _, _ = gan_model.train_on_batch(
					train_contours_batches[step], 
					[labels_real, train_faces_batches[step]])
			else:
				generator_out = g_model.predict(train_faces_batches[step])

				d_loss1 = d_model.train_on_batch(
					[train_faces_batches[step], train_contours_batches[step]], 
					labels_real)

				d_loss2 = d_model.train_on_batch(
					[train_faces_batches[step], generator_out], 
					labels_fake)

				g_loss, _, _ = gan_model.train_on_batch(
					train_faces_batches[step], 
					[labels_real, train_contours_batches[step]])

			print(f'Step discriminator loss real: {d_loss1}')
			print(f'Step discriminator loss fake: {d_loss2}')
			print(f'Step generator loss: {g_loss}')

			out = f2c.predict_img(
				g_model,
				C2F,
				TEST_IMG_LOAD_PATH)
			
			cv2.imwrite(f'{TEST_IMG_SAVE_PATH}/epoch_{epoch}_step_{step}.jpg', out)

			g_model.save(GENERATOR_SAVE_PATH)
			d_model.save(DISCRIMINATOR__SAVE_PATH)
		

dataset = prepare_dataset.load_dataset(
	CONTOURS_PATH, 
	FACES_PATH,
	TRAIN_SAMPLES)

if C2F == True:
	img_source_size = (256, 256, 1)
	img_target_size = (256, 256, 3)
	gen_output_channels = 3
else:
	img_source_size = (256, 256, 3)
	img_target_size = (256, 256, 1)
	gen_output_channels = 1

if FIRST_TRAINING == True:
	generator = contours_model.generator(img_source_size, gen_output_channels)
	discriminator = contours_model.discriminator(img_source_size, img_target_size)
	gan = contours_model.GAN(generator, discriminator, img_source_size)
else:
	generator = load_model(GENERATOR_LOAD_PATH)
	discriminator = load_model(DISCRIMINATOR__LOAD_PATH)
	gan = contours_model.GAN(generator, discriminator, img_source_size)

train(
	generator, 
	discriminator, 
	gan, 
	dataset,
	EPOCHS,
	BATCH_SIZE)
