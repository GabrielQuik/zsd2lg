import './App.css';
import Paint from './componets/pages/Paint';
import Contours from './componets/pages/Contours';
import Landing from './componets/pages/Landing'
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {

  return (
      <BrowserRouter>
        <Routes >
          <Route exact path="/" element={<Landing />} />
          <Route exact path="/paint" element={<Paint />} />
          <Route exact path="/contours" element={<Contours />} />
        </Routes>
      </BrowserRouter>
  );
}

export default App;
