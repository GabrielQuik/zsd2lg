import dlib
import os
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt

def generate_landmarks_curves(preds):
    """Connects generated landmark points into curves that are each a part of a face.

    :param preds: Object with generated landmarks
    :type preds: object
    ...
    :return: List of lists - each list is a face part
    :rtype: list
    """
    jaw = []
    left_brew = []
    right_brew = []
    nose = []
    left_eye = []
    right_eye = []
    lips = []
    mouth = []

    for i in range(17):
        jaw.append((preds.part(i).x, preds.part(i).y))

    for i in range(17, 22):
        left_brew.append((preds.part(i).x, preds.part(i).y))

    for i in range(22, 27):
        right_brew.append((preds.part(i).x, preds.part(i).y))

    for i in range(27, 36):
        nose.append((preds.part(i).x, preds.part(i).y))

    for i in range(36, 42):
        left_eye.append((preds.part(i).x, preds.part(i).y))
    left_eye.append((preds.part(36).x, preds.part(36).y))

    for i in range(42, 48):
        right_eye.append((preds.part(i).x, preds.part(i).y))
    right_eye.append((preds.part(42).x, preds.part(42).y))

    for i in range(48, 61):
        lips.append((preds.part(i).x, preds.part(i).y))
    lips.append((preds.part(48).x, preds.part(48).y))

    for i in range(60, 68):
        mouth.append((preds.part(i).x, preds.part(i).y))
    mouth.append((preds.part(60).x, preds.part(60).y))

    face = [jaw, left_brew, right_brew, left_eye, right_eye, nose, mouth, lips]
    return face

def create_landmark_contours_img(face,id):
    """Draws black contours of a face on a white background and saves it.

    :param face: list of lists of face curves
    :type preds: list
    :param id: image id
    :type preds: int
    """
    img = np.zeros((1024, 1024, 1), np.uint8)

    for line in face:
        x, y = zip(*line)
        for i in range(1, len(x)):
            cv2.line(img, (int(x[i - 1]), int(y[i - 1])), (int(x[i]), int(y[i])), (255, 255, 255), thickness=2)

    img=cv2.bitwise_not(img)
    cv2.imwrite(f'contours_out/landmarks_dlib/{id}', img)

def plot_landmarks(face,image):
    """Plots black contours of a face on a image

    :param face: list of lists of face curves
    :type preds: list
    :param image: original image from which the contours were generated
    :type preds: object
    """
    fig=plt.subplot(111)
    fig.imshow(image)
    for el in face:
        x,y=zip(*el)
        fig.plot(x,y)
    plt.show()

def main():
    """Main function that:
    - Loads landmarks detector
    - Iterates over all images in given path
    - Detects landmarks 
    - Generates curves and saves them
    """
    face_detector = dlib.get_frontal_face_detector()
    landmark_detector = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    path_to_faces = f'CelebAMask-HQ/CelebA-HQ-img/*.jpg'
    list_of_faces = glob.glob(path_to_faces)
    path_to_out=f'contours_out/landmarks_dlib/*.jpg'
    list_of_out=glob.glob(path_to_out)

    for el in list_of_faces:
        print(f'Przetwarzam zdjęcie {el}')
        if el[-6] == "/":
            id = el[-5:]
        elif el[-7]== "/":
            id = el[-6:]
        elif el[-8] == "/":
            id = el[-7:]
        elif el[-9] == "/":
            id = el[-8:]
        elif el[-10] == "/":
            id = el[-9:]
        else:
            id = el[-10:]
        out_str=f'contours_out/landmarks_dlib/{id}'

        if os.path.exists(out_str):
            print("zdjecie przetworzone")
        else:
            input = dlib.load_rgb_image(el)
            faces = face_detector(input, 1)

            if (len(faces) != 0):
                landmarks = landmark_detector(input, faces[0])
                face = generate_landmarks_curves(landmarks)
                create_landmark_contours_img(face, id)
            else:
                print("nie znaleziono twarzy w tym zdjęciu")

if __name__=="__main__":
   main()
