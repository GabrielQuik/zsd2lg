import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import Select from 'react-select'
import axios from "axios";
import FormData from 'form-data'
import Navbar from "../Navbar"
import Footer from "../Footer"


export default function Paint() {   

    const options = [
        { value: '#aa00ff', label: 'Right eye' },
        { value: '#00ff00', label: 'Left eye' },
        { value: '#5500ff', label: 'Left eyebrow' },
        { value: '#00aaff', label: 'Right eyebrow' },
        { value: '#55ff00', label: 'Left ear' },
        { value: '#00ffaa', label: 'Right ear' },
        { value: '#00007b', label: 'Nose' },
        { value: '#ff0055', label: 'Mouth' },
        { value: '#ff00aa', label: 'Upper lip' },
        { value: '#ff5500', label: 'Lower lip' },
        { value: '#55ffff', label: 'Cloth' },
        { value: '#aaffff', label: 'Hair' },
        { value: '#0055ff', label: 'Skin' },
        { value: '#ffaa00', label: 'Neck' }
    ]
    const canvasRef = useRef(null)
    const contextRef = useRef(null)
    const [isDrawing, setIsDrawing] = useState(false)

    useEffect(() => {
        const canvas = canvasRef.current;
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        let x = canvas.width/ 512;
        let y = canvas.height/512;
        canvas.style.width = `${window.innerWidth/x}px`;
        canvas.style.height = `${window.innerHeight/y}px`;
        console.log(canvas.width, canvas.height)
        const context = canvas.getContext("2d")
        context.scale(x, y)
        // const context = canvas.getContext("2d")
        // context.scale(3.75, 2.109375)
        context.lineCap = "round"
        context.strokeStyle = "black"
        context.lineWidth = 5
        contextRef.current = context;
    }, [])

    const startDrawing = ({ nativeEvent }) => {
        const { offsetX, offsetY } = nativeEvent;
        contextRef.current.beginPath()
        contextRef.current.moveTo(offsetX, offsetY)
        setIsDrawing(true)
    }
    const endDrawing = () => {
        contextRef.current.closePath()
        setIsDrawing(false)
    }

    const draw = ({ nativeEvent }) => {
        if (!isDrawing) {
            return
        }
        const { offsetX, offsetY } = nativeEvent;
        contextRef.current.lineTo(offsetX, offsetY)
        contextRef.current.stroke()

    }
    const changeColor = e => {
        contextRef.current.strokeStyle = e.value
    }
    const incrementWidth = () => {
        contextRef.current.lineWidth = contextRef.current.lineWidth + 1
    }
    const decrementWidth = () => {
        contextRef.current.lineWidth = contextRef.current.lineWidth - 1
    }
    const clear = () => {
        contextRef.current.clearRect(0, 0, contextRef.current.canvas.width, contextRef.current.canvas.height);
        document.getElementById("out").classList.remove("result_image");
    }
    const dowlnoad = async () => {
        var canvas = canvasRef.current
        canvas.toBlob(function(blob) {
            const formData = new FormData();
            formData.append('image', blob, 'filename.png');
            formData.append('mode', 2);
            axios.post('http://localhost:4000/', formData).then(() => {
                document.getElementById("out").classList.add("result_image");
            });
            
          });

    }

    const hiddenFileInput = React.useRef(null);

    const transform = event => {
        hiddenFileInput.current.click();  
    }

    const handleChange = event => {
        const fileUploaded = event.target.files[0];
        const formData = new FormData();
        formData.append('image', fileUploaded);
        formData.append('mode', 1);
        axios.post('http://localhost:4000/', formData).then(() => {document.getElementById("out").classList.add("result_image");});

    };

    const canvas_style = {
        border: "1px solid #000000",
        backgroundColor: "white",
        padding: "10px",
        fontFamily: "Arial"
        // backgroundImage TODO:
      };

    return (

        <div>
        <Navbar/>
        <div id="tools">
            <table>
                <tr>
                    <td></td>
                    Choose body part:
                    <div style={{ width: '300px' }}>
                        <Select options={options}
                            onChange={changeColor}
                        />
                    </div>

                    <td>
                        Line Width
                        <button onClick={incrementWidth}>+</button>
                        <button onClick={decrementWidth}>-</button></td>
                    <td><button onClick={clear}>Clear</button></td>
                    <td><button onClick={transform}>Transfrom from upload</button></td>
                    <input
                        type="file"
                        ref={hiddenFileInput}
                        onChange={handleChange}
                        style={{display: 'none'}}
                    />
                    <td> <button onClick={dowlnoad}>Transfrom from canvas</button> </td>
                </tr>
            </table>
        </div>
     
        <body>

            <div style = {{display: 'flex', justifyContent: 'center', margin: '30px 20px 0 0'}}>
            <canvas style={canvas_style}
                onMouseDown={startDrawing}
                onMouseMove={draw}
                onMouseUp={endDrawing}
                ref={canvasRef}>
            </canvas>
            <div id="out" className="result_frame"/>
            </div>

        </body>
        <Footer/>
        </div>
    )
}
