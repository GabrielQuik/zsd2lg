import tensorflow as tf
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import os
import pathlib
import time
import datetime
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
import argparse
from glob import glob 
BATCH_SIZE = 1
IMG_WIDTH = 512 #256
IMG_HEIGHT = 512 #256
OUTPUT_CHANNELS = 3
LAMBDA = 100

def Generator():
  """
    blocks in the encoder: Convolution -> Batch normalization -> Leaky ReLU
    blocks in the decoder: Transposed convolution -> Batch normalization -> Dropout (applied to the first 3 blocks) -> ReLU
    skip connections between the encoder and decoder 

  Returns:
      tf.keras.Model: Generator model
  """
  inputs = tf.keras.layers.Input(shape=[IMG_WIDTH, IMG_HEIGHT, 3])

  down_stack = [
    downsample(64, 4, apply_batchnorm=False),  # (batch_size, 128, 128, 64)
    downsample(128, 4),  # (batch_size, 64, 64, 128)
    downsample(256, 4),  # (batch_size, 32, 32, 256)
    downsample(512, 4),  # (batch_size, 16, 16, 512)
    downsample(512, 4),  # (batch_size, 8, 8, 512)
    downsample(512, 4),  # (batch_size, 4, 4, 512)
    downsample(512, 4),  # (batch_size, 2, 2, 512)
    downsample(512, 4),  # (batch_size, 1, 1, 512)
  ]

  up_stack = [
    upsample(512, 4, apply_dropout=True),  # (batch_size, 2, 2, 1024)
    upsample(512, 4, apply_dropout=True),  # (batch_size, 4, 4, 1024)
    upsample(512, 4, apply_dropout=True),  # (batch_size, 8, 8, 1024)
    upsample(512, 4),  # (batch_size, 16, 16, 1024)
    upsample(256, 4),  # (batch_size, 32, 32, 512)
    upsample(128, 4),  # (batch_size, 64, 64, 256)
    upsample(64, 4),  # (batch_size, 128, 128, 128)
  ]

  initializer = tf.random_normal_initializer(0., 0.02)
  last = tf.keras.layers.Conv2DTranspose(OUTPUT_CHANNELS, 4,
                                         strides=2,
                                         padding='same',
                                         kernel_initializer=initializer,
                                         activation='tanh')  # (batch_size, 256, 256, 3)

  x = inputs

  skips = []
  for down in down_stack:
    x = down(x)
    skips.append(x)

  skips = reversed(skips[:-1])

  for up, skip in zip(up_stack, skips):
    x = up(x)
    x = tf.keras.layers.Concatenate()([x, skip])

  x = last(x)

  return tf.keras.Model(inputs=inputs, outputs=x)


def downsample(filters, size, apply_batchnorm=True):
  """downsampler architecture
  Args:
      filters ([type]): num of filters in Conv2D layer
      size ([type]): size of filters
      apply_batchnorm (bool, optional): [description]. Defaults to True.

  Returns:
      tf.keras.Sequential : architecture of downsampler
                            ( Convolution -> Batch normalization -> Leaky ReLU)
  """
  initializer = tf.random_normal_initializer(0., 0.02)

  result = tf.keras.Sequential()
  result.add(
      tf.keras.layers.Conv2D(filters, size, strides=2, padding='same',
                             kernel_initializer=initializer, use_bias=False))

  if apply_batchnorm:
    result.add(tf.keras.layers.BatchNormalization())

  result.add(tf.keras.layers.LeakyReLU())

  return result

def upsample(filters, size, apply_dropout=False):
  """upsampler architecture

  Args:
      filters (int): num of filters
      size (int): size of filters in Conv2DTranspose layer
      apply_dropout (bool, optional): optional dropout layer . Defaults to False.

  Returns:
      tf.keras.Sequential :  upsampler layer
                            (Transposed convolution -> Batch normalization -> Dropout (applied to the first 3 blocks) -> ReLU)
  """
  initializer = tf.random_normal_initializer(0., 0.02)

  result = tf.keras.Sequential()
  result.add(
    tf.keras.layers.Conv2DTranspose(filters, size, strides=2,
                                    padding='same',
                                    kernel_initializer=initializer,
                                    use_bias=False))

  result.add(tf.keras.layers.BatchNormalization())

  if apply_dropout:
      result.add(tf.keras.layers.Dropout(0.5))

  result.add(tf.keras.layers.ReLU())

  return result

  
def Discriminator():
  """convolutional PatchGAN classifier
     blocks in the discriminator: Convolution -> Batch normalization -> Leaky ReLU.

  Returns:
      tf.keras.Model: discriminator model
  """
  initializer = tf.random_normal_initializer(0., 0.02)

  inp = tf.keras.layers.Input(shape=[IMG_WIDTH, IMG_HEIGHT, 3], name='input_image')
  tar = tf.keras.layers.Input(shape=[IMG_WIDTH, IMG_HEIGHT, 3], name='target_image')

  x = tf.keras.layers.concatenate([inp, tar])  # (batch_size, 256, 256, channels*2)

  down1 = downsample(64, 4, False)(x)  # (batch_size, 128, 128, 64)
  down2 = downsample(128, 4)(down1)  # (batch_size, 64, 64, 128)
  down3 = downsample(256, 4)(down2)  # (batch_size, 32, 32, 256)

  zero_pad1 = tf.keras.layers.ZeroPadding2D()(down3)  # (batch_size, 34, 34, 256)
  conv = tf.keras.layers.Conv2D(512, 4, strides=1,
                                kernel_initializer=initializer,
                                use_bias=False)(zero_pad1)  # (batch_size, 31, 31, 512)

  batchnorm1 = tf.keras.layers.BatchNormalization()(conv)

  leaky_relu = tf.keras.layers.LeakyReLU()(batchnorm1)

  zero_pad2 = tf.keras.layers.ZeroPadding2D()(leaky_relu)  # (batch_size, 33, 33, 512)

  last = tf.keras.layers.Conv2D(1, 4, strides=1,
                                kernel_initializer=initializer)(zero_pad2)  # (batch_size, 30, 30, 1)

  return tf.keras.Model(inputs=[inp, tar], outputs=last) 

if __name__ == '__main__':
  generator = Generator()
  discriminator = Discriminator()


  parser = argparse.ArgumentParser()
  parser.add_argument('image_paths', type=str, nargs='+',
                      help='first argument - path to file , second argument - destination file path')

  parser.add_argument('mode',type=str, nargs='+',
                      help='seg - segments , con - contours' )
  
  parser.add_argument('direction',type=str, nargs='+',
                      help='AB - segments or contours to faces, BA - faces to segments or contours' )

  args = parser.parse_args()
  image_file = args.image_paths[0] 
  dest_file = args.image_paths[1] 
  mode = args.mode[0]

  if args.direction[0] == 'AB':
    checkpoint_dir = '/home/komati/Uczelnia/x/zsd2lg/p2p_tf/training_checkpoints_AB_segments_new/' if 'seg' in mode else '/home/komati/Uczelnia/x/zsd2lg/p2p_tf/training_checkpoints_AB_landmarks/'
  else:
    checkpoint_dir = '/home/komati/Uczelnia/x/zsd2lg/p2p_tf/training_checkpoints_BA_segments_new/' if 'seg' in mode else '/home/komati/Uczelnia/x/zsd2lg/p2p_tf/training_checkpoints_BA_landmarks/'

  checkpoint = tf.train.Checkpoint(generator=generator,
                                  discriminator=discriminator)

  checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

  image = tf.io.read_file(image_file)
  image = tf.image.decode_jpeg(image)
  input_image = tf.cast(image, tf.float32)
  input_image = tf.image.resize(input_image, [IMG_HEIGHT, IMG_WIDTH], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
  # if mode == 'con' and  args.direction[0] == 'BA':
  # input_image = np.stack((input_image,)*3, axis = -2)
  input_image = input_image[:,:,:3]
  input_image = (input_image / 127.5) - 1
  input_imgs = []
  input_imgs.append(input_image)
  input_imgs = np.asarray(input_imgs)
  
  prediction = generator(input_imgs, training = True)
  x = prediction[0]*0.5 + 0.5
  im = Image.fromarray(np.asarray(x*255, dtype=np.uint8))
  im.save(dest_file)
