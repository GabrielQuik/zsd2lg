import numpy as np
import cv2
import os


def find_data(begin, stop, files_edges, files_targets, edges_path, targets_path):
    ds_e = []
    ds_t = []

    for i in range(begin, stop):
        edge_name = files_edges[files_edges.index(f'hed_{i}.jpg')]
        edge_path = f'{edges_path}/{edge_name}'
        im_e = cv2.imread(edge_path, cv2.IMREAD_GRAYSCALE)
        im_e = cv2.resize(im_e, (256, 256))
        im_e = (127.5 - im_e) / 127.5
        ds_e.append(im_e)

        target_idx = [s.replace('.jpg', '') for s in files_targets]
        target_idx = list(map(int, target_idx))
        target_name = files_targets[target_idx.index(i+1)]
        target_path = f'{targets_path}/{target_name}'
        im_t = cv2.imread(target_path)
        im_t = cv2.resize(im_t, (256, 256))
        im_t = (127.5 - im_t) / 127.5
        ds_t.append(im_t)

    ds_e = np.array(ds_e)
    ds_t = np.array(ds_t)

    return ds_e, ds_t


def load_dataset(edges_path, targets_path, train_samples=24000, valid_samples=0, test_samples=0):
    files_edges = os.listdir(edges_path)
    files_targets = os.listdir(targets_path)

    ds_train_e, ds_train_t = find_data(
        0, 
        train_samples, 
        files_edges, 
        files_targets,
        edges_path,
        targets_path)
    ds_valid_e, ds_valid_t = find_data(
        train_samples, 
        train_samples + valid_samples, 
        files_edges, 
        files_targets,
        edges_path,
        targets_path)
    ds_test_e, ds_test_t = find_data(
        train_samples + valid_samples, 
        train_samples + valid_samples + test_samples, 
        files_edges, 
        files_targets,
        edges_path,
        targets_path)

    return [ds_train_e, ds_train_t, ds_valid_e, ds_valid_t, ds_test_e, ds_test_t]

