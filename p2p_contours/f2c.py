from keras.models import load_model
import numpy as np
import cv2
import argparse
import sys 


GENERATOR_PATH = '../p2p_contours/training_checkpoints_BA_landmarks.h5'


def load_generator(gen_path=GENERATOR_PATH):
    """Returns model of generator.

    :param gen_path: Path to the generator model, defaults to GENERATOR_PATH
    :type gen_path: str, optional
    :return: Model of generator
    :rtype: keras.engine.functional.Functional
    """
    return load_model(gen_path)


def predict_img(generator, c2f, img_path):
    """Processes specified face image to contours image and vice versa using 
    chosen generator model.

    :param generator: Model of generator
    :type generator: keras.engine.functional.Functional
    :param c2f: 'True' if process contours to faces, 'False' otherwise
    :type c2f: bool
    :param img_path: The path to the image to be processed
    :type img_path: str
    :return: Processed image
    :rtype: numpy.ndarray
    """
    if c2f == True:
        im = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        im = cv2.bitwise_not(im)
    else:
        im = cv2.imread(img_path)
    im = cv2.resize(im, (256, 256))
    im = (127.5 - im) / 127.5
    im = np.expand_dims(im, axis=0)

    out = generator.predict(im)
    out = np.squeeze(out, axis=0)
    out = (out + 1.0) / 2.0
    out = 255*out
    out = out.astype(np.uint8)
    if c2f == True:
        out = cv2.bitwise_not(out)
    
    return out


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Processing a face image into a contour image.')
    parser.add_argument(
        'path_to_source_file', 
        type=str, 
        help='The path to the image to be processed.')
    parser.add_argument(
        'path_to_dest_file', 
        type=str,
        help='The storage path of the processed image.')
    args = parser.parse_args()

    gen = load_generator()
    out = predict_img(gen, False, args.path_to_source_file)
    out = cv2.resize(out, (512,512), interpolation = cv2.INTER_AREA)
    cv2.imwrite(args.path_to_dest_file, out)
