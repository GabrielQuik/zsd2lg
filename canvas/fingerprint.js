var drawing = false;
var context;
var brushType=['pen','circle','square']

var e = document.getElementById("faceElement");

window.onload=function()
{
    
    const download = document.getElementById('download');
    document.getElementById('btnDownload').addEventListener('click', function(){
        const link = document.createElement('a');
        link.download = 'download.png';
        link.href = myCanvas.toDataURL();
        link.click();
        link.delete;
      }, false);

    document.getElementById('btnClear').addEventListener('click', function(){
            context.clearRect(0,0, context.canvas.width, context.canvas.height);       
        }, false);
    
 
        //circle
        document.getElementById('circle').addEventListener('click', function(){
            brushType='circle'     
        }, false);
    
        //square
        document.getElementById('square').addEventListener('click', function(){
            brushType='square'
        }, false);
    
        //pen 
        document.getElementById('pen').addEventListener('click', function(){
            brushType='pen'  
        }, false);
     
    
    //Width Scale
    document.getElementById('lineWidth').addEventListener('change', function(){
        
    drawing = false;
            context.lineWidth = document.getElementById('lineWidth').value;
        }, false);
    
  
    //Size Canvas
    context = document.getElementById('myCanvas').getContext("2d");
    context.canvas.width = window.innerWidth;
    context.canvas.height = window.innerHeight-60;
    
    //Mouse movement
    document.onmousemove = handleMouseMove;
    document.onmousedown = handleDown;
    document.onmouseup = handleUp;
    
    //Style line
    context.strokeStyle = "#000";
    context.lineJoin = "round";
    context.lineWidth = 5;
    
    //Hide Save Area
    document.getElementById('saveArea').style.display = "none";
}

function handleMouseMove(e)
{
    console.log(e.clientX);
    console.log(e.clientY);
    if(drawing)
    {
        if(brushType=='circle'&&drawing)
{
        context.beginPath();
        context.arc(e.clientX,e.clientY,context.lineWidth/4,0,Math.PI*2,false);
        context.fill();
        context.stroke();
}
        else if(brushType=='pen'&&drawing)
    {
            context.lineTo(e.clientX, e.clientY);
        context.closePath();
        context.stroke();
        context.moveTo(e.clientX, e.clientY);
    
    }
    else if(brushType=='square'){

        context.beginPath();
        context.fillRect(e.clientX-context.lineWidth, e.clientY-context.lineWidth, context.lineWidth, context.lineWidth);
        context.stroke();
    }
}
    else
    {
        context.moveTo(e.clientX, e.clientY);
    }
    document.getElementById('faceElement').addEventListener('click', function(){
        if(document.getElementById('faceElement').value=='rightEye')
        {
             context.strokeStyle= '#00fa74';}
        else  if(document.getElementById('faceElement').value=='leftEye')
        {context.strokeStyle= '#00fee0';}
        else  if(document.getElementById('faceElement').value=='leftEyebrow')
        {context.strokeStyle='#0011ff';}
        else  if(document.getElementById('faceElement').value=='rightEyebrow')
        {context.strokeStyle='#00c7ff';}
        else  if(document.getElementById('faceElement').value=='glasses')
        {context.strokeStyle='#26ef00';}
        else  if(document.getElementById('faceElement').value=='leftEar')
        {context.strokeStyle='#6ede00';}
        else  if(document.getElementById('faceElement').value=='rightEar')
        {context.strokeStyle='#97ff1d';}
        else  if(document.getElementById('faceElement').value=='nose')
        {context.strokeStyle='#ffc607';}
        else  if(document.getElementById('faceElement').value=='mouth')
        {context.strokeStyle='#642b02';}
        else  if(document.getElementById('faceElement').value=='upperLip')
        {context.strokeStyle= '#ff1c00';}
        else  if(document.getElementById('faceElement').value=='lowerLip')
        {context.strokeStyle='#ff00ba';}
        else  if(document.getElementById('faceElement').value=='cloth')
        {context.strokeStyle='#5f4760';}
        else  if(document.getElementById('faceElement').value=='hair')
        {context.strokeStyle='#fef7fe';}
        else  if(document.getElementById('faceElement').value=='skin')
        {context.strokeStyle='#015718';}
        else  if(document.getElementById('faceElement').value=='neck')
        {context.strokeStyle='#b624ff';}
        drawing=false;
}, false);
    
}

function handleDown(e)
{
    drawing = !drawing; 
    console.log(drawing);
    context.moveTo(e.clientX, e.clientY);
    context.beginPath();
     
}

function handleUp()
{
    drawing = !drawing;
    console.log(drawing);
}