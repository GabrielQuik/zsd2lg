package service

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/gofiber/fiber/v2"
)

type ServerService struct {
}

func NewServerService() *ServerService {
	serverController := new(ServerService)
	return serverController
}

func (service ServerService) Write(c *fiber.Ctx) error {
	file, err := c.FormFile("image")
	if err != nil {
		log.Println("image download error --> ", err)
		return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})

	}

	err = c.SaveFile(file, fmt.Sprintf("./img.png"))

	if err != nil {
		log.Println("image save error --> ", err)
		return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
	}

	mode := c.FormValue("mode")

	imageUrl := fmt.Sprintf("http://localhost:4000/images/%s", "img.png")

	data := map[string]interface{}{

		"imageName": "img.png",
		"imageUrl":  imageUrl,
		"header":    file.Header,
		"size":      file.Size,
	}

	pwd, err := os.Getwd()
	if err != nil {
		log.Println("getting path error --> ", err)
		return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
	}
	app := "python"
	arg1 := strings.Replace(pwd, "backend", "p2p_tf/test.py", 1)
	arg2 := pwd + "/img.png"
	arg3 := strings.Replace(pwd, "backend", "pix2pix/src/imgs/result.png", 1)
	arg4 := " "
	arg5 := " "


	switch mode {
	case "1":
		arg4 = "seg"
		arg5 = "BA"
		out, err := exec.Command(app, arg1, arg2, arg3, arg4, arg5).Output()
		if err != nil {
			log.Println("image processing error --> ", err)
			return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
		}

		fmt.Println(string(out))
	case "2":
		arg4 = "seg"
		arg5 = "AB"
		out, err := exec.Command(app, arg1, arg2, arg3, arg4, arg5).Output()
		if err != nil {
			log.Println("image processing error --> ", err)
			return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
		}

		fmt.Println(string(out))
	case "3":
		arg1 := strings.Replace(pwd, "backend", "p2p_contours/f2c.py", 1)
		// arg2 := pwd + "/img.png"
		// arg3 := strings.Replace(pwd, "backend", "pix2pix/src/imgs/result.png", 1)

		out, err := exec.Command(app, arg1, arg2, arg3).Output()
		
		if err != nil {
			log.Println("image processing error --> ", err)
			return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
		}
		
		log.Println(string(out))

			
	
	case "4":
		arg4 = "con"
		arg5 = "AB"
		out, err := exec.Command(app, arg1, arg2, arg3, arg4, arg5).Output()
		if err != nil {
			log.Println("image processing error --> ", err)
			return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
		}

		fmt.Println(string(out))
	}

	return c.JSON(fiber.Map{"status": 201, "message": "Image uploaded successfully", "data": data})
}
